import Types from '../../action/types';

const defaultState ={}
/**
 * popular:{
 *  Java:{
 *      items:[],
 *      isLoading:false
 *  },
 *  React:{
 *      items:[],
 *      isLoading:false
 *  }
 * }
 * state树 横向扩展
 * 如何动态的设置store 和动态获取store (难点 storekey 不固定)
 * @param {*} state 
 * @param {*} action 
 */
export default function onAction(state = defaultState, action){
    switch(action.type){
        case Types.TRENDING_REFRESH_SUCCESS: //下拉刷新成功
            return {
                ...state,
                [action.storeName]:{
                    ...state[action.storeName],
                    items: action.items,//原始数据
                    projectModes:action.projectModes,//此次要展示的数据
                    isLoading:false,
                    hideLoadingMore: false,
                    pageIndex: action.pageIndex
                }
            }
        case Types.TRENDING_REFRESH: 
            return {
                ...state,
                [action.storeName]:{
                    ...state[action.storeName],
                    isLoading:true,
                    hideLoadingMore:true
                }
            }
        case Types.TRENDING_REFRESH_FAIL: //下拉刷新失败
            return {
                ...state,
                [action.storeName]:{
                    ...state[action.storeName],
                    isLoading:false
                }
            }
        case Types.TRENDING_LOAD_MORE_SUCCESS: //
            return {
                ...state, //Object.assign
                [action.storeName]:{
                    ...state[action.storeName],
                    projectModes:action.projectModes,
                    hideLoadingMore:false,
                    pageIndex: action.pageIndex
                }
            }
            case Types.TRENDING_LOAD_MORE_FAIL: //下拉刷新成功
            return {
                ...state, //Object.assign
                [action.storeName]:{
                    ...state[action.storeName],
                    hideLoadingMore:true,
                    pageIndex: action.pageIndex
                }
            }
        default:
            return state;
    }
}