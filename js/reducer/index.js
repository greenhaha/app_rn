import {combineReducers} from 'redux';
import theme from './theme';
import popular from './popular';
import trending from './trending'
import {rootCom,RootNavigator} from '../navigations/Navigations';
// 指定默认state
const navState = RootNavigator.router.getStateForAction(RootNavigator.router.getActionForPathAndParams(rootCom))
/**
 * 2.创建自己的navigation reducer
 * @param {*} state 
 * @param {*} action 
 */
const navReducer = (state = navState, action) =>{
    const nextState = RootNavigator.router.getStateForAction(action, state);
    // 如果 nextState 为null 或者没有定义  只需要返回原始 state
    return nextState || state;
}
/**
 * 合并reducer
 * @type {Reducer<any>| Reducer<any, AnyAction>}
 */
const index = combineReducers({
    nav:navReducer,
    theme: theme,
    popular:popular,
    trending: trending,
})

export default index;