/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, TouchableOpacity,SafeAreaView} from 'react-native';
import NavigationBar from '../common/NavigationBar';
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'
const THEME_COLOR = '#678'
type Props = {};
export default class MyPage extends Component<Props> {
  getLeftButton(){
    return (
      <TouchableOpacity
        style={{padding: 8,paddingLeft: 12}}
      >
        <Ionicons 
          name={'ios-arrow-back'}
          size={26}
          style={{color:'white'}}
        />
      </TouchableOpacity>
    );
  }
  getRightButton(){
    return (
      <View style={{flexDirection:'row'}}>
        <TouchableOpacity
          onPress={()=>{}}
        >
        <View style={{padding: 5, marginRight:8}}>
            <Feather 
              name={'search'}
              size={24}
              style={{color: 'white'}}
            />
        </View>
        </TouchableOpacity>
      </View>
    );
  }
  render() {
    let statusBar={
      backgroundColor:THEME_COLOR,
      barStyle:'light-content'
    }
    let navigationBar = <NavigationBar
      title={'我的'}
      statusBar = {statusBar}
      style={{backgroundColor:THEME_COLOR}}
      rightButton={this.getRightButton()}
      leftButton = {this.getLeftButton()}
    />
    return (
      <View style={styles.container}>
      <SafeAreaView>
        {navigationBar}
        </SafeAreaView>
        <Text style={styles.welcome}>MyPage</Text>
        <Text onPress={()=>{
              NavigationUtil.goPage({
                navigation: this.props.navigation
              },"DetailPage")
          }}>跳转到详情页</Text>
          <Button 
            title={'Fetch 使用'}
            onPress={()=>{
              NavigationUtil.goPage({
                navigation: this.props.navigation
              },"FetchDemoPage")
            }}
          />
          <Button 
            title={'AsyncStore 使用'}
            onPress={()=>{
              NavigationUtil.goPage({
                navigation: this.props.navigation
              },"AsyncStoreDemoPage")
            }}
          />
          <Button 
            title={'离线缓存框架'}
            onPress={()=>{
              NavigationUtil.goPage({
                navigation: this.props.navigation
              },"DataStoreDemo")
            }}
          />
          
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },

});
