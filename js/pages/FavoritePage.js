

import React, {Component} from 'react';
import {Platform,Button, StyleSheet, Text, View} from 'react-native';
import {connect} from 'react-redux';
import actions from '../action'

type Props = {};
class FavoritePage extends Component<Props> {
  render() {
    const {navigation} = this.props
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>FavoritePage</Text>
        <Button 
            title = "改变主题颜色"
            onPress={()=>{
               this.props.onThemeChange('#006')
            }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
const mapStateToProps =state=>({})

const mapDispatchToProps = dispatch=>({
  onThemeChange: theme=>dispatch(actions.onThemeChange(theme))
})

export default  connect(mapStateToProps,mapDispatchToProps)(FavoritePage)
