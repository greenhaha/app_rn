
// import React, {Component} from 'react';
// import {Platform,Button, StyleSheet, Text, View} from 'react-native';
// import {connect} from 'react-redux';
// import actions from '../action'
// type Props = {};
// class TrendingPage extends Component<Props> {
//   render() {
//       const {navigation} = this.props
//     return (
//       <View style={styles.container}>
//         <Text style={styles.welcome}>TrendingPage</Text>
//         <Button 
//             title = "改变主题颜色"
//             onPress={()=>{
//                 this.props.onThemeChange('#096')
//             }}
//         />
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
// });
// const mapStateToProps =state=>({})

// const mapDispatchToProps = dispatch=>({
//   onThemeChange: theme=>dispatch(actions.onThemeChange(theme))
// })

// export default  connect(mapStateToProps,mapDispatchToProps)(TrendingPage)

import React, {Component} from 'react';
import {Platform, StyleSheet, Text,Button, View,SafeAreaView, FlatList, RefreshControl, ActivityIndicator} from 'react-native';
import {
    createMaterialTopTabNavigator,
    createAppContainer
} from 'react-navigation'
import actions from '../action/index'
import {connect} from 'react-redux';
import TrendingItem from '../common/TrendingItem';
import Toast from 'react-native-easy-toast'
type Props = {};
import NavigationUtil from '../navigations/NavigationUtils'
import { onLoadPopularData } from '../action/popular';
import NavigationBar from '../common/NavigationBar'
const URL = 'https://github.com/trending/'
const QUERY_STR = '&since=daily'
const THEME_COLOR = '#678'
export default class TrendingPage extends Component<Props> {
    constructor(props){
        super(props)
        this.tabNames = ["All","C","C#","PHP", "JavaScript"]
    }
    _genTab(){
        const tabs = {};
        this.tabNames.forEach((item, index)=>{
            tabs[`tab${index}`]={
                screen: props=><TrendingTabPage {...props} tabLabel={item}/>,
                navigationOptions:{
                    title:item
                }
            }
        });
        return tabs;
    }
    _tabTopRender(){
        return createMaterialTopTabNavigator(this._genTab(),{
            tabBarOptions:{
                upperCaseLabel:false, // 是否标签大写  默认true
                tabStyle:styles.tabStyle,
                scrollEnabled: true, // 是否支持选项卡滚动 默认false
                style:{
                    backgroundColor: '#678', // tabBar 的背景颜色
                    height: 30
                },
                indicatorStyle:styles.indicatorStyle, // 标签指示器的样式
                labelStyle: styles.labelStyle //文字的样式
            }
        })
    }
  render() {
    let statusBar={
      backgroundColor:THEME_COLOR,
      barStyle:'light-content'
    }
    let navigationBar = <NavigationBar
      title={'趋势'}
      statusBar = {statusBar}
      style={{backgroundColor:THEME_COLOR}}
    />
      const TabTop = createAppContainer(this._tabTopRender())
    return (
        <SafeAreaView style={{flex:1}}>
        {navigationBar}
      <TabTop />
      </SafeAreaView>
    );
  }
}
const pageSize = 10
class TrendingTab extends Component<Props> {
  constructor(props){
    super(props)
    const {tabLabel} = this.props
    this.storeName = tabLabel
  }
  componentDidMount(){
    this.loadData()
  }
  loadData(loadMore){
    const {onRefreshTrending, onLoadMoreTrending} = this.props
    const store = this._store();
    const url = this.genFetchUrl(this.storeName)
    if(loadMore){
      onLoadMoreTrending(this.storeName,++store.pageIndex,pageSize,store.items, callback=>{
        this.refs.toast.show('没有更多了')
      })
    }else {
      onRefreshTrending(this.storeName, url, pageSize)
    }
    
  }
  _store(){
    const {trending} = this.props
    let store = trending[this.storeName]
    if(!store){
      store={
        items: [],
        isLoading:false,
        projectModes: [],//要现实的数据
        hideLoadingMore: true,//默认加载更多
      }
    }
    return store
  }
  genFetchUrl(key){
    return URL+key+QUERY_STR
  }
  renderItem(data){
    const item = data.item
    return <TrendingItem 
            item={item}
            onSelect={()=>{

            }}
        />
  }
  genIndicator(){
    return this._store().hideLoadingMore?null:
    <View style={styles.indicatorContainer}>
      <ActivityIndicator 
        style={styles.indicator}
      />
      <Text>正在加载更多</Text>
    </View>
  }
  render() {
      let store = this._store()
    return (
      <View style={styles.container}>
          <FlatList 
            data={store.projectModes}
            renderItem={data=>this.renderItem(data)}
            keyExtractor={item=>''+(item.id|| item.fullName)}
            refreshControl={
              <RefreshControl 
                title={'loading'}
                titleColor={THEME_COLOR}
                colors={[THEME_COLOR]}
                refreshing={store.isLoading}
                onRefresh={()=>this.loadData()}
                tintColor={THEME_COLOR}
              />
            }
            ListFooterComponent={()=>this.genIndicator()}
            onEndReached={()=>{
              setTimeout(()=>{
                if(this.canLoadMore){
                  this.loadData(true)
                  this.canLoadMore=false
                }
              }, 100)              
            }}
            onEndReachedThreshold={0.5}
            onMomentumScrollBegin={()=>{
              this.canLoadMore = true
            }}

          />
          <Toast 
            ref={'toast'}
            position={'center'}
          />
      </View>
    );
  }
}
const mapStateToProps = state =>({
  trending: state.trending
})
const mapDispatchToProps = dispatch=>({
  onRefreshTrending: (storeName, url,pageSize)=>dispatch(actions.onRefreshTrending(storeName, url, pageSize)),
  onLoadMoreTrending:(storeName,pageIndex, pageSize, items, callBack)=>dispatch(actions.onLoadMoreTrending(storeName,pageIndex, pageSize, items, callBack))
})
const TrendingTabPage = connect(mapStateToProps, mapDispatchToProps)(TrendingTab)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  tabStyle:{
      //minWidth: 50,
      padding: 0

  },
  indicatorStyle:{
    height:2,
    backgroundColor:'white'
  },
  labelStyle:{
      fontSize:13,
      margin:0
  },
  indicatorContainer:{
    alignItems:'center'
  },
  indicator:{
    color: 'red',
    margin: 10
  }
});

