
import React, {Component} from 'react';
import {Platform, StyleSheet, Text,Button, View,SafeAreaView, FlatList, RefreshControl, ActivityIndicator,DeviceInfo} from 'react-native';
import {
    createMaterialTopTabNavigator,
    createAppContainer
} from 'react-navigation'
import actions from '../action/index'
import {connect} from 'react-redux';
import PopularItem from '../common/PopularItem';
import Toast from 'react-native-easy-toast'
type Props = {};
import NavigationUtil from '../navigations/NavigationUtils'
import { onLoadPopularData } from '../action/popular';
import NavigationBar from '../common/NavigationBar'
const URL = 'https://api.github.com/search/repositories?q='
const QUERY_STR = '&sort=stars'
const THEME_COLOR = '#678'
export default class PopularPage extends Component<Props> {
    constructor(props){
        super(props)
        this.tabNames = ["Java","Android","iOS","React","React Native", "PHP"]
    }
    _genTab(){
        const tabs = {};
        this.tabNames.forEach((item, index)=>{
            tabs[`tab${index}`]={
                screen: props=><PopularTabPage {...props} tabLabel={item}/>,
                navigationOptions:{
                    title:item
                }
            }
        });
        return tabs;
    }
    _tabTopRender(){
        return createMaterialTopTabNavigator(this._genTab(),{
            tabBarOptions:{
                upperCaseLabel:false, // 是否标签大写  默认true
                tabStyle:styles.tabStyle,
                scrollEnabled: true, // 是否支持选项卡滚动 默认false
                style:{
                    backgroundColor: '#678', // tabBar 的背景颜色
                    height:30
                },
                indicatorStyle:styles.indicatorStyle, // 标签指示器的样式
                labelStyle: styles.labelStyle //文字的样式
            }
        })
    }
  render() {
    let statusBar={
      backgroundColor:THEME_COLOR,
      barStyle:'light-content'
    }
    let navigationBar = <NavigationBar
      title={'最热'}
      statusBar = {statusBar}
      style={{backgroundColor:THEME_COLOR}}
    />
      const TabTop = createAppContainer(this._tabTopRender())
    return (
        <SafeAreaView style={{flex:1}}>
        {navigationBar}
      <TabTop />
      </SafeAreaView>
    );
  }
}
const pageSize = 10
class PopularTab extends Component<Props> {
  constructor(props){
    super(props)
    const {tabLabel} = this.props
    this.storeName = tabLabel
  }
  componentDidMount(){
    this.loadData()
  }
  loadData(loadMore){
    const {onRefreshPopular, onLoadMorePopular} = this.props
    const store = this._store();
    const url = this.genFetchUrl(this.storeName)
    if(loadMore){
      onLoadMorePopular(this.storeName,++store.pageIndex,pageSize,store.items, callback=>{
        this.refs.toast.show('没有更多了')
      })
    }else {
      onRefreshPopular(this.storeName, url, pageSize)
    }
    
  }
  _store(){
    const {popular} = this.props
    let store = popular[this.storeName]
    if(!store){
      store={
        items: [],
        isLoading:false,
        projectModes: [],//要现实的数据
        hideLoadingMore: true,//默认加载更多
      }
    }
    return store
  }
  genFetchUrl(key){
    return URL+key+QUERY_STR
  }
  renderItem(data){
    const item = data.item
    return <PopularItem 
            item={item}
            onSelect={()=>{

            }}
        />
  }
  genIndicator(){
    return this._store().hideLoadingMore?null:
    <View style={styles.indicatorContainer}>
      <ActivityIndicator 
        style={styles.indicator}
      />
      <Text>正在加载更多</Text>
    </View>
  }
  render() {
      let store = this._store()
    return (
      <View style={styles.container}>
          <FlatList 
            data={store.projectModes}
            renderItem={data=>this.renderItem(data)}
            keyExtractor={item=>''+item.id}
            refreshControl={
              <RefreshControl 
                title={'loading'}
                titleColor={THEME_COLOR}
                colors={[THEME_COLOR]}
                refreshing={store.isLoading}
                onRefresh={()=>this.loadData()}
                tintColor={THEME_COLOR}
              />
            }
            ListFooterComponent={()=>this.genIndicator()}
            onEndReached={()=>{
              setTimeout(()=>{
                if(this.canLoadMore){
                  this.loadData(true)
                  this.canLoadMore=false
                }
              }, 100)              
            }}
            onEndReachedThreshold={0.5}
            onMomentumScrollBegin={()=>{
              this.canLoadMore = true
            }}

          />
          <Toast 
            ref={'toast'}
            position={'center'}
          />
      </View>
    );
  }
}
const mapStateToProps = state =>({
  popular: state.popular
})
const mapDispatchToProps = dispatch=>({
  onRefreshPopular: (storeName, url,pageSize)=>dispatch(actions.onRefreshPopular(storeName, url, pageSize)),
  onLoadMorePopular:(storeName,pageIndex, pageSize, items, callBack)=>dispatch(actions.onLoadMorePopular(storeName,pageIndex, pageSize, items, callBack))
})
const PopularTabPage = connect(mapStateToProps, mapDispatchToProps)(PopularTab)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  tabStyle:{
      //minWidth: 50,
      padding: 0

  },
  indicatorStyle:{
    height:2,
    backgroundColor:'white'
  },
  labelStyle:{
      fontSize:13,
      margin:0
  },
  indicatorContainer:{
    alignItems:'center'
  },
  indicator:{
    color: 'red',
    margin: 10
  }
});
