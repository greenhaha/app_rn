

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, BackHandler} from 'react-native';
import {
    createBottomTabNavigator,
    createAppContainer,
    NavigationActions
} from 'react-navigation'
import PopularPage from './PopularPage';
import FavoritePage from './FavoritePage';
import MyPage from './MyPage';
import TrendingPage from './TrendingPage';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Entypo from 'react-native-vector-icons/Entypo'
import NavigationUtils from '../navigations/NavigationUtils';
import DynamicTabNavigator from '../navigations/DynamicTabNavigator';
import {connect} from 'react-redux'
type Props = {};
class HomePage extends Component<Props> {
  componentDidMount(){
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }
  componentWillUnmount(){
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress)
  }
  /**
   * 处理安卓无力返回键
   * @returns {boolean}
   */
  onBackPress =()=>{
    const {dispatch, nav} = this.props
    if(nav.routes[1].index === 0){
      return false
    }
    dispatch(NavigationActions.back());
    return true;
  }
  render() {
    NavigationUtils.navigation = this.props.navigation
    return (
      <DynamicTabNavigator />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
const mapStateToProps =state=>({
  nav: state.nav
})

export default  connect(mapStateToProps)(HomePage)
