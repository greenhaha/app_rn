/**
 * 全局导航跳转工具类
 */
export default class NavigationUtils{
    /**
     * 跳转到指定页面
     * @param {*} params 要传递的参数
     * @param {*} page 要跳转的页面
     */
    static goPage(params, page){
        const navigation = NavigationUtils.navigation
        if(!navigation){
            console.log('navigation can not be null')
            return;
        }
        navigation.navigate(
            page,
            {
                ...params
            }
        )
    }
    /**
     * 返回上一个页面
     * @param {*} navigation 
     */
    static goBack(navigation){
        navigation.goBack()
    }
    static resectToHomePage(params){
        const {navigation} = params;
        navigation.navigate("Main")
    }
}