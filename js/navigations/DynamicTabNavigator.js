

import React, {Component} from 'react';

import {
    createBottomTabNavigator,
    createAppContainer
} from 'react-navigation'
import PopularPage from '../pages/PopularPage';
import FavoritePage from '../pages/FavoritePage';
import MyPage from '../pages/MyPage';
import TrendingPage from '../pages/TrendingPage';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Entypo from 'react-native-vector-icons/Entypo'
import NavigationUtils from '../navigations/NavigationUtils';
import {BottomTabBar} from 'react-navigation-tabs'; 
import {connect} from 'react-redux'
const TABS = { //在这里配置路由页面
    PopularPage:{
        screen: PopularPage,
        navigationOptions:{
            tabBarLabel:'最热',
            tabBarIcon:({tintColor, focused})=>(
                <MaterialIcons
                    name={'whatshot'}
                    size={26}
                    style={{color:tintColor}}
                />
            )
        }
    },
    TrendingPage:{
        screen: TrendingPage,
        navigationOptions:{
            tabBarLabel:'趋势',
            tabBarIcon:({tintColor, focused})=>(
                <Ionicons
                    name={'md-trending-up'}
                    size={26}
                    style={{color:tintColor}}
                />
            )
        }
    },
    FavoritePage:{
        screen: FavoritePage,
        navigationOptions:{
            tabBarLabel:'收藏',
            tabBarIcon:({tintColor, focused})=>(
                <MaterialIcons
                    name={'favorite'}
                    size={26}
                    style={{color:tintColor}}
                />
            )
        }
    },
    MyPage:{
        screen: MyPage,
        navigationOptions:{
            tabBarLabel:'我的',
            tabBarIcon:({tintColor, focused})=>(
                <Entypo
                    name={'user'}
                    size={26}
                    style={{color:tintColor}}
                />
            )
        }
    },
}
type Props = {};
class DynamicTabNavigator extends Component<Props> {
    constructor(props){
        super(props);
        console.disableYellowBox = true //  隐藏到 警告信息
    }
    _taBarBottom(){
        if(this.Tab){
            return this.Tab
        }
        const {PopularPage, TrendingPage, FavoritePage, MyPage} = TABS
        const tabs={PopularPage, TrendingPage, FavoritePage, MyPage} //根据需要动态定制显示的tab
        PopularPage.navigationOptions.tabBarLabel = '最新' // 动态配置Tab属性
        return this.Tab = createAppContainer(createBottomTabNavigator(tabs,{
            tabBarComponent:props=>{
                return <TabBarComponent theme={this.props.theme} {...props}/>
            }
        })) 
    }
  render() {
    const Tab = this._taBarBottom()
    return (
      <Tab />
    );
  }
}
class TabBarComponent extends React.Component{
    constructor(props){
        super(props);
        this.theme = {
            tintColor: props.activeTintColor,
            updateTime: new Date().getTime()
        }
    }
    render(){
        return (
            <BottomTabBar 
                {...this.props}
                activeTintColor = {this.props.theme}
            />
        )
    }
}
const mapStateToProps = state =>({
    theme: state.theme.theme
})
export default connect(mapStateToProps)(DynamicTabNavigator)