/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './js/App';
import {createAppContainer} from 'react-navigation'
import {name as appName} from './app.json';

const AppContainer= createAppContainer(App)
AppRegistry.registerComponent(appName, () => App);
